<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

echo "<h2> HEWAN 1</h2>";
$sheep = new animal('shaun');

echo "Nama Hewan : ". $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki: ". $sheep->legs . "<br>"; // 2
echo "Berdarah Dingin : ". $sheep->cold_blooded . "<br><br>";// false

echo "<h2> HEWAN 2</h2>";

$monkey = new ape("kera sakti");
echo "Nama Hewan : ". $monkey->name . "<br>"; // "shaun"
echo "Jumlah Kaki: ". $monkey->legs . "<br>"; // 2
echo "Berdarah Dingin : ". $monkey->cold_blooded . "<br>";// false
echo $monkey->yell(). "<br><br>"; // "Auooo"

echo "<h2> HEWAN 3</h2>";

$kodok = new frog("buduk");

echo "Nama Hewan : ". $kodok->name . "<br>"; // "shaun"
echo "Jumlah Kaki: ". $kodok->legs . "<br>"; // 2
echo "Berdarah Dingin : ". $kodok->cold_blooded . "<br>";// false
echo $kodok->jump(). "<br>" ; // "hop hop"
?>