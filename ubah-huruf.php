<?php
    function ubah_huruf($string){
        $arrayhuruf = str_split($string);

        $berubah = "";
        foreach ($arrayhuruf as $tmp){
        $berubah .= chr(ord($tmp)+1);
    }
    echo $berubah . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>